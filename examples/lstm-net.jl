using Strada

batchsize = 15
timelen = 5

data = MemoryLayer(:data; shape=(timelen, batchsize), stream_axis=1)
label = MemoryLayer(:label; shape=(timelen, batchsize), stream_axis=1)
cont = MemoryLayer(:cont; shape=(timelen, batchsize), stream_axis=1)
embed = EmbedLayer(:embed, [data]; input_dim=10, num_output=10)
lstm = LSTMLayer(:lstm, [embed, cont]; num_output=10, weight_filler=filler(:uniform, min=-0.08, max=0.08))
linear = LinearLayer(:linear, [lstm]; n_filter=10, axis=2)
loss = SoftmaxWithLoss(:loss, [linear, label]; axis=2)

net = Net("LSTMNet", [data, label, cont], loss; log_level=3)
