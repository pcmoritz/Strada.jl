using Strada
using Logging

include("svm-net.jl")

filename = joinpath(Pkg.dir("Strada"), "data", "a9a")
batchsize = 100

(X, y) = read_svmlight(filename)
(p, n) = size(X)

X = X[:, 1:100]
y = reshape(y[1:100], 1, 100)

y += 1 # make -1, 0, 1 into labels

data = minibatch_stream(full(X), y; batchsize=100)

net = make_svm(p)

Logging.configure(level=INFO)

(objective, theta) = make_objective(net, Float32)
predictor = make_predictor(net, Float32, :linear)

theta = sgd(objective, data, theta; predictor=predictor, testset=data, lr_schedule=ConstLR(10.0, 0.9), epochs=30)
