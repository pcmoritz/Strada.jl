using Strada

function make_svm(p::Int; batchsize::Int=100)
	data = MemoryLayer(:data; shape=(batchsize, 1, 1, p))
	label = MemoryLayer(:label; shape=(batchsize, 1))
	linear = LinearLayer(:linear, [data]; n_filter=3, weight_filler=filler(:constant))
	loss = SoftmaxWithLoss(:loss, [linear, label])
	return Net("SVMNet", [data, label], loss; log_level=3)
end
