using Strada

batchsize = 64

data = MemoryLayer(:data; shape=(batchsize, 1, 28, 28))
label = MemoryLayer(:label; shape=(batchsize, 1))
conv1 = ConvLayer(:conv1, [data]; kernel=(5,5), n_filter=20)
pool1 = PoolLayer(:pool1, [conv1]; kernel=(2,2), stride=(2,2))
conv2 = ConvLayer(:conv2, [pool1]; kernel=(5,5), n_filter=50)
pool2 = PoolLayer(:pool2, [conv2]; kernel=(2,2), stride=(2,2))
ip1 = LinearLayer(:ip1, [pool2]; n_filter=500)
relu1 = ActivationLayer(:relu1, [ip1]; activation=ReLU)
ip2 = LinearLayer(:ip2, [relu1]; n_filter=10)
loss = SoftmaxWithLoss(:loss, [ip2, label])

net = Net("LeNet", [data, label], loss; log_level=3);
