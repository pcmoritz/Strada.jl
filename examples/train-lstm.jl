using Strada
using Logging

include("lstm-net.jl")

(objective, theta) = make_objective(net, Float32)
predictor = make_predictor(net, Float32, :linear)

X = floor(10 * rand(Float32, timelen, 10*batchsize))
C = zeros(Float32, timelen, 10*batchsize)
C[2:timelen,:] = 1.0

data = minibatch_stream(X, X, C; batchsize=batchsize)

Logging.configure(level=INFO)

theta = sgd(objective, data, theta; testset=data, predictor=predictor, lr_schedule=InvLR(0.1, 0.0001, 0.75, 0.9), epochs=20, verbose=true)

result = zeros(Int, timelen, batchsize)
predictor((X, X, C), theta; result=result)

