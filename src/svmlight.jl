using GZip

function read_svmlight(filename::String, Dtype::DataType=Float32)
	f = GZip.open(filename)
	lines = readlines(f)
	lines = lines[1:end-1]
	n = length(lines)
	y = zeros(Dtype, n)
	I = Int[]
	J = Int[]
	V = Dtype[]
	for (i, line) in enumerate(lines)
		data = split(line)
		y[i] = parse(Dtype, data[1])
		for d in data[2:end]
			(idx, val) = split(d, ":")
			push!(I, parse(Int, idx))
			push!(J, i)
			push!(V, parse(Dtype, val))
		end
	end
	close(f)
	return (sparse(I, J, V), y)
end
