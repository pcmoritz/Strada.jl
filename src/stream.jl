abstract DataStream

type EmptyStream <: DataStream
end

getfull(x::AbstractVector, I) = x[I]
getfull(x::AbstractMatrix, I) = x[:, I]
getfull{F}(x::AbstractArray{F, 3}, I) = x[:, :, I]
getfull{F}(x::AbstractArray{F, 4}, I) = x[:, :, :, I]

type MinibatchStream <: DataStream
	data::Vector{Data}
    batchsize::Int

	function MinibatchStream(input::Data, batchsize::Int, drop::Bool=true)
		for i in 1:length(input)-1
			size(input[i], ndims(input[i])) == size(input[i+1], ndims(input[i+1])) ||
				throw(DimensionMismatch("All input arrays must have the same number of samples."))
		end
		n = size(input[1], ndims(input[1]))
		@assert mod(n, batchsize) == 0 || drop
		data = Data[]
		for i = 1:div(n, batchsize)
			range = ((i-1) * batchsize + 1) : i * batchsize
			push!(data, tuple([getfull(input[j], range) for j in 1:length(input)]...))
		end
		new(data, batchsize)
	end
end

minibatch_stream(args::AbstractArray...; batchsize::Int=1) = begin
	for i in 1:length(args) - 1
		eltype(args[i]) == eltype(args[i+1]) ||
			error("All input arrays must have the same type")
	end
	MinibatchStream(args, batchsize, true)
end

start(str::MinibatchStream) = 1
done(str::MinibatchStream, s) = num_batches(str) + 1 == s
next(str::MinibatchStream, s) = (str.data[s], s+1)

function num_batches(str::MinibatchStream)
	return length(str.data)
end

function size(str::MinibatchStream)
    return num_batches(str) * str.batchsize
end

function get_batchsize(str::MinibatchStream)
	return str.batchsize
end

function getminibatch(str::MinibatchStream)
	i = rand(1:size(str.xs, 1))
	return (str.xs[i], str.ys[i])
end
