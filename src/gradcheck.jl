function make_stencil{F}(theta::Vector{F}, epsilon::F)
	delta = deepcopy(theta)
	rand!(delta)
	first = deepcopy(theta)
	BLAS.axpy!(epsilon, delta, first)
	second = deepcopy(theta)
	BLAS.axpy!(-epsilon, delta, second)
	return (delta, first, second)
end

function grad_check{F}(objective::Function, theta::Vector{F}, data::Data{F}, epsilon::Float64; abs_error::Bool=true, verbose::Bool=false)
	epsilon = convert(F, epsilon)
	(delta, first, second) = make_stencil(theta, epsilon)
	deriv_approx = (objective(data, first) - objective(data, second))/(2*epsilon)
	grad = deepcopy(theta)
	objective(data, theta; grad=grad)
	deriv_exact = dot(grad, delta)
	if verbose
		println("exact  derivative: ", deriv_exact)
		println("finite diff derivative: ", deriv_approx)
	end
	if abs_error
		return abs(deriv_approx - deriv_exact)
	else
		return abs((deriv_approx - deriv_exact)/deriv_exact)
	end
end

function single_grad{F}(objective::Function, data::Data{F}, theta::Vector{F})
	grad = deepcopy(theta)
	objective(data, theta; grad=grad)
	return grad
end

function hess_check{F}(objective::Function, theta::Vector{F}, data::Data{F}, epsilon::F; abs_error::Bool=true)
	(delta, first, second) = make_stencil(theta, epsilon)
	hvp_exact = deepcopy(theta)
	objective(data, theta; direction=delta, hvp=hvp_exact)
	hvp_approx = single_grad(objective, data, first) - single_grad(objective, data, second)
	BLAS.scale!(hvp_approx, 1/(2*epsilon))
	# TODO: report error
end
