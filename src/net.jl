using DataStructures

type CaffeNet
	param::NetParameter
	layer_defs::Vector{Layer}
	state::Ptr{Void}
	blobs::Blobs
	layers::Blobs
	num_data_layers::Int
end

function make_net_param(layers::Vector{LayerParameter})
	net_param = NetParameter()
	set_field!(net_param, :layer, layers)
	return net_param
end

function collect_layers(layer::Layer, accum::Vector{Layer})
	push!(accum, layer)
	for child in layer.bottoms
		collect_layers(child, accum)
	end
end

function Net(name::ASCIIString, data_layers::Vector{Layer}, loss::Layer; log_level::Int=3)
	layers = Layer[]
	collect_layers(loss, layers)
	layers = filter(function (x) !(x in data_layers) end, layers)
	# ensure that the data layers are at the beginning, in the same order as in data_layers
	layer_list = [data_layers; Base.reverse(layers)]
	param = make_net_param([layer.param for layer in layer_list])
	buffer = PipeBuffer()
	writeproto(buffer, param)
	state = ccall((:init_net, "libjlcaffe.so"), Ptr{Void}, (Ptr{Void}, Int, Int), buffer.data, length(buffer.data), log_level)
	return CaffeNet(param, layer_list, state, get_blobs(state), get_layer_blobs(state), length(data_layers))
end

function load_caffemodel(net::CaffeNet, filename::String)
	ccall((:load_from_caffemodel, "libjlcaffe.so"), Void, (Ptr{Void}, Ptr{UInt8}), net.state, filename)
end

function set_data!{N}(net::CaffeNet, data::Array{Float32, N}, layer_idx::Int)
	stream_axis = net.layer_defs[layer_idx].stream_axis
	stream_axis = stream_axis == -1 ? ndims(data) : stream_axis
	n = size(data, stream_axis)
	# make layer_idx compatible with caffe's zero indexing
	ccall((:set_data, "libjlcaffe.so"), Void, (Ptr{Void}, Int, Ptr{Float32}, Int), net.state, layer_idx - 1, pointer(data), n)
end

function get_batchsize(net::CaffeNet)
	return net.param.input_shape[1].dim[end]
end

function forward(net::CaffeNet)
	ccall((:forward, "libjlcaffe.so"), Void, (Ptr{Void},), net.state)
end

function backward(net::CaffeNet)
	ccall((:backward, "libjlcaffe.so"), Void, (Ptr{Void},), net.state)
end

function reverse(net::CaffeNet)
	ccall((:reverse, "libjlcaffe.so"), Void, (Ptr{Void},), net.state)
end

function get_shape(blob)
	num_axes = ccall((:get_num_axes, "libjlcaffe.so"), Int, (Ptr{Void},), blob)
	result = Int[]
	for axis = 0:num_axes-1
		axis_shape = ccall((:get_axis_shape, "libjlcaffe.so"), Int, (Ptr{Void},Int), blob, axis)
		push!(result, axis_shape)
	end
	return tuple(result...)
end

function push_data!(blob::Blob, field::ASCIIString, data::Ptr{Float32}, shape::Tuple)
	if !haskey(blob.vars, field)
		blob.vars[field] = []
	end
	push!(blob.vars[field], pointer_to_array(data, Base.reverse(shape))) # row major and column major
end

function assign_blob!(source::Ptr{Void}, field::ASCIIString, target::Blobs)
	shape = get_shape(source)
	data = ccall((:get_data, "libjlcaffe.so"), Ptr{Float32}, (Ptr{Void},), source)
	diff = ccall((:get_diff, "libjlcaffe.so"), Ptr{Float32}, (Ptr{Void},), source)
	inc_data = ccall((:get_inc_data, "libjlcaffe.so"), Ptr{Float32}, (Ptr{Void},), source)
	inc_diff = ccall((:get_inc_diff, "libjlcaffe.so"), Ptr{Float32}, (Ptr{Void},), source)
	push_data!(target.data, field, data, shape)
	push_data!(target.diff, field, diff, shape)
	push_data!(target.inc_data, field, inc_data, shape)
	push_data!(target.inc_diff, field, inc_diff, shape)
end

function get_layer_blobs(state::Ptr{Void})
	result = Blobs()
	num_layers = ccall((:num_layers, "libjlcaffe.so"), Int, (Ptr{Void},), state)
	for layer = 0:num_layers-1
		name = ccall((:layer_name, "libjlcaffe.so"), Ptr{UInt8}, (Ptr{Void}, Int), state, layer)
		num_axis = ccall((:num_blob_axis, "libjlcaffe.so"), Int, (Ptr{Void}, Int), state, layer)
		for axis in 0:num_axis-1
			blob = ccall((:get_weight_blob, "libjlcaffe.so"), Ptr{Void}, (Ptr{Void}, Int, Int), state, layer, axis)
			assign_blob!(blob, bytestring(name), result)
		end
	end
	return result
end

function get_blobs(state::Ptr{Void})
	result = Blobs()
	num_blobs = ccall((:num_blobs, "libjlcaffe.so"), Int, (Ptr{Void},), state)
	for blob_idx = 0:num_blobs-1
		name = bytestring(ccall((:get_blob_name, "libjlcaffe.so"), Ptr{UInt8}, (Ptr{Void}, Int), state, blob_idx))
		blob = ccall((:get_blob, "libjlcaffe.so"), Ptr{Void}, (Ptr{Void}, Int), state, blob_idx)
		assign_blob!(blob, bytestring(name), result)
	end
	return result
end
