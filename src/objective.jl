using Compat
using ArrayViews

function make_objective(net::CaffeNet, F::DataType; lambda::Float64=5e-4, loss_layer_name::Symbol=:loss)
	@assert F == Float32 # so far, only Float32 is supported
	lambda = convert(F, lambda)
	N = net.num_data_layers
	function objective(data::Data{F,N}, theta::Vector{F};
		grad::Vector{F}=F[], direction::Vector{F}=F[], hvp::Vector{F}=F[])
		for i = 1:N
			set_data!(net, data[i], i)
		end
		zero!(net.layers.diff)
		vec2blob!(theta, net.layers.data)
		l2 = convert(F, 0.5) * lambda * dot(theta, theta)
		forward(net)
		if length(grad) > 0 || length(hvp) > 0 # calc grad and hvp
			backward(net)
		end
		if length(grad) > 0 # calc grad
			blob2vec!(net.layers.diff, grad)
			BLAS.axpy!(lambda, theta, grad) # add regularizer
		end
		if length(hvp) > 0 # calc hvp
			vec2blob!(direction, net.layers.inc_data)
			reverse(net)
			blob2vec!(net.layers.inc_diff, hvp)
			BLAS.axpy!(lambda, direction, hvp) # add regularizer
		end
		return net.blobs.data.vars[string(loss_layer_name)][1][1] + l2
	end

	blob = net.layers.data
	theta = zeros(F, length(blob))
	blob2vec!(blob, theta)

	return (objective, theta)
end

function argmax_zero!(a::DenseArray, result::DenseArray)
    for i = 1:length(result)
     	result[i] = indmax(view(a, :, i)) - 1
    end
end

function make_predictor(net::CaffeNet, F::DataType, output_layer_name::Symbol; loss_layer_name::Symbol=:loss)
	@assert F == Float32 # again, only Float32 is supported at the moment
	N = net.num_data_layers
	# for result, the shape is (T, N)
	function predictor(data::Data{F,N}, theta::Vector{F}; result::Matrix{Int}=zeros(Int, 0, 0))
		for i = 1:N
			set_data!(net, data[i], i)
		end
		vec2blob!(theta, net.layers.data)
		forward(net)
		pred = net.blobs.data.vars[string(output_layer_name)][1]
		if length(result) > 0
				argmax_zero!(pred, result)
		end
		return net.blobs.data.vars[string(loss_layer_name)][1][1]
	end
	return predictor
end
