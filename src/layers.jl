using Compat

type Layer
	name::Symbol
	bottoms::Vector{Layer}
	param_name::Symbol
	param::LayerParameter
	stream_axis::Int
end

function make_param(lr_mult::Float64)
	param = ParamSpec()
	set_field!(param, :lr_mult, @compat Float32(lr_mult))
	return param
end

function make_layer(typ::ASCIIString, name::Symbol, bottoms::Vector{Layer}, tops::Vector{Symbol}; has_weights=false)
	bottom_symbols = [layer.name for layer in bottoms]
	layer = LayerParameter()
	set_field!(layer, :name, string(name))
	set_field!(layer, :_type, typ)
	bottom_names = length(bottom_symbols) == 0 ? ASCIIString[] : map(string, bottom_symbols)
	top_names = length(tops) == 0 ? ASCIIString[] : map(string, tops)
	set_field!(layer, :bottom, convert(Vector{AbstractString}, bottom_names))
	set_field!(layer, :top, convert(Vector{AbstractString}, top_names))
	if has_weights
		param1 = make_param(1.0)
		param2 = make_param(2.0)
		set_field!(layer, :param, [param1, param2])
	end
	return layer
end

function filler(name::Symbol; value::Float64=NaN, min::Float64=NaN, max::Float64=NaN, mean::Float64=NaN, std::Float64=NaN)
	result = FillerParameter()
	function set_num!(field::Symbol, value::Float64)
		if !isnan(value)
			set_field!(result, field, @compat Float32(value))
		end
	end
	set_field!(result, :_type, string(name))
	set_num!(:value, value)
	set_num!(:min, min)
	set_num!(:max, max)
	set_num!(:mean, mean)
	set_num!(:std, std)
	return result
end

function ConvLayer(name::Symbol, bottoms::Vector{Layer};
		kernel::NTuple{2,Int}=(1,1), stride::NTuple{2,Int}=(1,1), pad::NTuple{2,Int}=(0,0), n_filter::Int=1,
		weight_filler::FillerParameter=filler(:xavier),
		bias_filler::FillerParameter=filler(:constant))
	layer = make_layer("Convolution", name, bottoms, [name]; has_weights=true)
	conv_param = ConvolutionParameter()
	set_field!(conv_param, :num_output, @compat UInt32(n_filter))
	set_field!(conv_param, :kernel_h, @compat UInt32(kernel[1]))
	set_field!(conv_param, :kernel_w, @compat UInt32(kernel[2]))
	set_field!(conv_param, :stride_h, @compat UInt32(stride[1]))
	set_field!(conv_param, :stride_w, @compat UInt32(stride[2]))
	set_field!(conv_param, :pad_h, @compat UInt32(pad[1]))
	set_field!(conv_param, :pad_w, @compat UInt32(pad[2]))
	set_field!(conv_param, :weight_filler, weight_filler)
	set_field!(conv_param, :bias_filler, bias_filler)
	set_field!(conv_param, :bias_term, true)
	set_field!(layer, :convolution_param, conv_param)
	return Layer(name, bottoms, :convolution_param, layer, 0)
end

function LinearLayer(name::Symbol, bottoms::Vector{Layer};
		n_filter::Int=1, weight_filler::FillerParameter=filler(:xavier),
		bias_filler::FillerParameter=filler(:constant), axis::Int=-1)
	layer = make_layer("InnerProduct", name, bottoms, [name]; has_weights=true)
	linear_param = InnerProductParameter()
	set_field!(linear_param, :num_output, @compat UInt32(n_filter))
	set_field!(linear_param, :weight_filler, weight_filler)
	set_field!(linear_param, :bias_filler, bias_filler)
	set_field!(linear_param, :bias_term, true)
	if axis != -1
		set_field!(linear_param, :axis, @compat Int32(axis))
	end
	set_field!(layer, :inner_product_param, linear_param)
	return Layer(name, bottoms, :inner_product_param, layer, 0)
end

function PoolLayer(name::Symbol, bottoms::Vector{Layer};
		method::Int=0, kernel::NTuple{2, Int}=(1,1), stride::NTuple{2, Int} = (1,1))
	layer = make_layer("Pooling", name, bottoms, [name])
	pool_param = PoolingParameter()
	set_field!(pool_param, :pool, @compat Int32(method))
	set_field!(pool_param, :kernel_h, @compat UInt32(kernel[1]))
	set_field!(pool_param, :kernel_w, @compat UInt32(kernel[2]))
	set_field!(pool_param, :stride_h, @compat UInt32(stride[1]))
	set_field!(pool_param, :stride_w, @compat UInt32(stride[2]))
	set_field!(layer, :pooling_param, pool_param)
	return Layer(name, bottoms, :pooling_param, layer, 0)
end

immutable Activation
	activation::Symbol
	param::Symbol
end

ReLU = Activation(:ReLU, :relu_param)
Sigmoid = Activation(:Sigmoid, :sigmoid_param)

function ActivationLayer(name::Symbol, bottoms::Vector{Layer};
	activation::Activation=Sigmoid)
	layer = make_layer(string(activation.activation), name, bottoms, [name]) 
	return Layer(name, bottoms, activation.param, layer, 0)
end

const WITHIN_CHANNEL = 1 # TODO: Read that out from the proto file

function LRNLayer(name::Symbol, bottoms::Vector{Layer};
		local_size::Int=3, alpha::Float64=5e-5, beta::Float64=0.75, norm_region=WITHIN_CHANNEL)
	layer = make_layer("LRN", name, bottoms, [name])
	lrn_param = LRNParameter()
	set_field!(lrn_param, :local_size, @compat UInt32(local_size))
	set_field!(lrn_param, :alpha, @compat Float32(alpha))
	set_field!(lrn_param, :beta, @compat Float32(beta))
	set_field!(lrn_param, :norm_region, @compat Int32(norm_region))
	set_field!(layer, :lrn_param, lrn_param)
	return Layer(name, bottoms, :lrn_param, layer, 0)
end

function LSTMLayer(name::Symbol, bottoms::Vector{Layer};
		num_output::Int=1, weight_filler::FillerParameter=filler(:xavier), bias_filler::FillerParameter=filler(:constant))
	layer = make_layer("LSTM", name, bottoms, [name])
	recurrent_param = RecurrentParameter()
	set_field!(recurrent_param, :num_output, @compat UInt32(num_output))
	set_field!(recurrent_param, :weight_filler, weight_filler)
	set_field!(recurrent_param, :bias_filler, bias_filler)
	set_field!(layer, :recurrent_param, recurrent_param)
	return Layer(name, bottoms, :recurrent_param, layer, 0)
end

function EmbedLayer(name::Symbol, bottoms::Vector{Layer};
		input_dim::Int=1, num_output::Int=1)
	layer = make_layer("Embed", name, bottoms, [name])
	embed_param = EmbedParameter()
	set_field!(embed_param, :input_dim, @compat UInt32(input_dim))
	set_field!(embed_param, :num_output, @compat UInt32(num_output))
	set_field!(layer, :embed_param, embed_param)
	return Layer(name, bottoms, :embed_param, layer, 0)
end

function SoftmaxWithLoss(name::Symbol, bottoms::Vector{Layer}; axis::Int=-1)
	layer = make_layer("SoftmaxWithLoss", name, bottoms, [name])
	softmax_param = SoftmaxParameter()
	if axis != -1
		set_field!(softmax_param, :axis, @compat Int32(axis))
		set_field!(layer, :softmax_param, softmax_param)
	end
	return Layer(name, bottoms, :softmax_param, layer, 0)
end

function HingeLoss(name::Symbol, bottoms::Vector{Layer})
	layer = make_layer("HingeLoss", name, bottoms, [name])
	return Layer(name, bottoms, :hinge_loss_param, layer, 0)
end

function MemoryLayer{N}(name::Symbol; shape::NTuple{N, Int}=(1,1,1,1), stream_axis::Int=-1)
	layer = make_layer("PointerData", name, Layer[], [name])
	memory_data_param = PointerDataParameter()
	shape_param = BlobShape()
	set_field!(shape_param, :dim, [shape...])
	set_field!(memory_data_param, :shape, shape_param)
	set_field!(layer, :pointer_data_param, memory_data_param)
	return Layer(name, Layer[], :pointer_data_param, layer, stream_axis)
end
