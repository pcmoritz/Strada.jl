using DataStructures
using Base.Random
import Base.fill!, Base.copy!, Base.Random.rand!, Base.dot, Base.length

type Blob
	vars::OrderedDict{ASCIIString, Vector{Any}}
end

Blob() = begin
	return Blob(OrderedDict{ASCIIString, Vector{Any}}())
end

type Blobs
	data::Blob
	diff::Blob
	inc_data::Blob
	inc_diff::Blob
end

Blobs() = begin
	return Blobs(Blob(), Blob(), Blob(), Blob())
end

function length(blob::Blob)
	len = 0
	for key in keys(blob.vars)
		for i in 1:length(blob.vars[key])
			len += length(blob.vars[key][i])
		end
	end
	return len
end

function blob2vec!(input::Blob, output::Vector)
	curr_idx = 1
	for key in keys(input.vars)
		for i in 1:length(input.vars[key])
			curr_len = length(input.vars[key][i])
			copy!(sub(output, curr_idx:(curr_idx+curr_len)-1), input.vars[key][i])
			curr_idx += curr_len
		end
	end
end

function vec2blob!(input::Vector, output::Blob)
	curr_idx = 1
	for key in keys(output.vars)
		for i in 1:length(output.vars[key])
			curr_len = length(output.vars[key][i])
			copy!(output.vars[key][i], sub(input, curr_idx:(curr_idx+curr_len)-1))
			curr_idx += curr_len
		end
	end
end

function copy!(A::Blob, B::Blob)
	for key in keys(B.vars)
		for i in 1:length(B.vars[key])
			copy!(A.vars[key][i], B.vars[key][i])
		end
	end
end

function zero!(A::Blob)
	for key in keys(A.vars)
		for i in 1:length(A.vars[key])
			fill!(A.vars[key][i], 0.0)
		end
	end
end
