module Strada

import Base.start
import Base.done
import Base.next
import Base.size
import Logging.info
using Compat

export
	MemoryLayer, ConvLayer, PoolLayer, LinearLayer, ActivationLayer, ReLU, Sigmoid, LRNLayer, LSTMLayer,
		EmbedLayer, SoftmaxWithLoss, HingeLoss, filler,
	CaffeNet, Net, forward, backward, reverse, set_data!, layers, blobs, WITHIN_CHANNEL, get_batchsize,
	Data, Blob, copy!, zero!, length, vec2blob!, blob2vec!,
	DataStream, minibatch_stream, num_batches, get_batchsize, getminibatch,
	sgd, InvLR, ConstLR,
	grad_check, hess_check, make_objective, make_predictor,
	read_svmlight

typealias Data{F,N} NTuple{N, AbstractArray{F}}

include("caffe_pb.jl")
include("error.jl")
include("layers.jl")
include("blobs.jl")
include("net.jl")
include("prettyprint.jl")
include("objective.jl")
include("gradcheck.jl")
include("svmlight.jl")
include("stream.jl")
include("sgd.jl")
include("utils.jl")

libpath = joinpath(Pkg.dir("Strada"), "deps", "usr", "lib")
Libdl.dlopen(joinpath(libpath, "libjlcaffe.so"))

# load blas and export symbols
Libdl.dlopen(Base.libblas_name, Libdl.RTLD_LAZY | Libdl.RTLD_DEEPBIND | Libdl.RTLD_GLOBAL)

# Initialize caffe
ccall((:init_jlcaffe, "libjlcaffe.so"), Void, ())
ccall((:set_global_error_callback, "libjlcaffe.so"), Void, (Ptr{Void},), error_callback_c)

end # module
