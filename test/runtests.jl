using Strada
using Base.Test

include("test-grad-mnist.jl")
include("test-grad-cifar.jl")
include("test-grad-svm.jl")
