#include "caffe/net.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/proto/caffe.pb.h"
#include <string>
#include <vector>
#include <stdexcept>
#include <boost/shared_ptr.hpp>

#include <glog/logging.h>

#include "jlcaffe.h"

struct net_state {
	caffe::Net<DTYPE> *net;
};

void init_jlcaffe() {
	google::InitGoogleLogging("");
}

net_state* init_net(const char* net_param, int net_param_len, int log_verbosity) {
	google::SetStderrLogging(log_verbosity);
	net_state *state = new net_state();
	caffe::NetParameter param;
	param.ParseFromString(std::string(net_param, net_param_len));
	state->net = new caffe::Net<DTYPE>(param);
	return state;
}

const char* layer_name(net_state* state, int i) {
	return state->net->layers()[i]->layer_param().name().c_str();
}

int num_layers(net_state* state) {
	return state->net->layers().size();
}

int num_blob_axis(net_state* state, int layer_idx) {
	return state->net->layers()[layer_idx]->blobs().size();
}

void* get_blob(net_state* state, int blob_idx) {
	return state->net->blobs()[blob_idx].get();
}

const char* get_blob_name(net_state* state, int blob_idx) {
	return state->net->blob_names()[blob_idx].c_str();
}

void* get_weight_blob(net_state* state, int layer_idx, int blob_idx) {
	return state->net->layers()[layer_idx]->blobs()[blob_idx].get();
}

void* get_output_blob(net_state* state, int blob_idx) {
	return state->net->output_blobs()[blob_idx];
}

void* get_input_blob(net_state* state, int blob_idx) {
	return state->net->input_blobs()[blob_idx];
}

int num_blobs(net_state* state) {
	return state->net->blobs().size();
}

int num_input_blobs(net_state* state) {
	return state->net->num_inputs();
}

int num_output_blobs(net_state* state) {
	return state->net->num_outputs();
}

#define TO_BLOB(blob) ((caffe::Blob<DTYPE>*)blob)

DTYPE* get_data(void* blob) {
	return TO_BLOB(blob)->mutable_cpu_data();
}

DTYPE* get_diff(void* blob) {
	return TO_BLOB(blob)->mutable_cpu_diff();
}

DTYPE* get_inc_data(void* blob) {
	// return TO_BLOB(blob)->mutable_cpu_inc_data();
}

DTYPE* get_inc_diff(void* blob) {
	// return TO_BLOB(blob)->mutable_cpu_inc_diff();
}

int get_num_axes(void* blob) {
	return TO_BLOB(blob)->num_axes();
}

int get_axis_shape(void* blob, int axis) {
	return TO_BLOB(blob)->shape(axis);
}

void set_data(net_state* state, int layer_idx, DTYPE* data_arr, int num_data) {
	if (state->net->layers().size() == 0) {
		throw std::runtime_error("tried to set data of empty network");
	}
	boost::shared_ptr<caffe::PointerDataLayer<DTYPE> > md_layer =
		boost::dynamic_pointer_cast<caffe::PointerDataLayer<DTYPE> >(state->net->layers()[layer_idx]);
	if (!md_layer) {
		throw std::runtime_error("set_input_arrays may only be called if the"
		" respective layer is a MemoryDataLayer");
	}
	md_layer->Reset(data_arr, num_data);
}

void forward(net_state* state) {
	int start_ind = 0;
	int end_ind = state->net->layers().size() - 1;
	state->net->ForwardFromTo(start_ind, end_ind);
}

void backward(net_state* state) {
	int start_ind = 0;
	int end_ind = state->net->layers().size() - 1;
	state->net->BackwardFromTo(end_ind, start_ind);
}

void reverse(net_state* state) {
	// state->net->RvForwardBackward();
}

void destroy_net(net_state* state) {
	delete state->net;
	delete state;
}

void set_global_error_callback(callback c) {
	global_caffe_error_callback = c;
}

void load_from_caffemodel(net_state* state, const char* filename) {
	state->net->CopyTrainedLayersFrom(filename);
}