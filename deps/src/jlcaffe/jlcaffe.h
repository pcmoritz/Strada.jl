#define DTYPE float

extern "C" {
	typedef void (*callback)(const char *msg);

	callback global_caffe_error_callback;

	struct net_state;

	void init_jlcaffe();

	net_state* init_net(const char* net_param, int net_param_len, int log_verbosity);
	void set_data(net_state* state, int layer_idx, DTYPE* data_arr, int num_data);
	const char * layer_name(net_state* state, int i);
	void forward(net_state* state);
	void backward(net_state* state);
	void reverse(net_state* state);

	int num_layers(net_state* state);
	int num_blob_axis(net_state* state, int layer_idx);

	int num_blobs(net_state* state);
	int num_input_blobs(net_state* state);
	int num_output_blobs(net_state* state);

	void* get_blob(net_state* state, int blob_idx);
	const char* get_blob_name(net_state* state, int blob_idx);
	void* get_weight_blob(net_state* state, int layer_idx, int blob_idx);
	void* get_input_blob(net_state* state, int blob_idx);
	void* get_output_blob(net_state* state, int blob_idx);

	DTYPE* get_data(void* blob);
	DTYPE* get_diff(void* blob);
	DTYPE* get_inc_data(void* blob);
	DTYPE* get_inc_diff(void* blob);

	int get_num_axes(void* blob);
	int get_axis_shape(void* blob, int axis);

	void set_global_error_callback(callback c);

	void load_from_caffemodel(net_state* state, const char* filename);
}
